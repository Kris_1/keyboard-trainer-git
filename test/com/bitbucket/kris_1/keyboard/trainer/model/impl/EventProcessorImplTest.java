package com.bitbucket.kris_1.keyboard.trainer.model.impl;

import static java.lang.Math.*;
import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.junit.Test;

import com.bitbucket.kris_1.keyboard.trainer.model.Event;

public class EventProcessorImplTest
{
    double getBool() {
        // double r = 0;
        // for (int i = 0; i < 12; i++)
        // r += random();
        double r = random();
        return (random() > 0.65 ? 1 : -1) * r;
    }

    char getChar() {
        return (char) ('a' + (int) (('z' - 'a' + 1) * random()));
    }

    @Test
    public void test() {
        EventProcessorImpl ep = new EventProcessorImpl(EventProcessorImpl.of('a', 'z'), 60000, 10);
        System.out.println(new Date());
        // for (int i = 0; i < 100; i++)
        // System.out.println(getChar());
        for (int j = 0; j < 100000; j++) {
            ep.addEvent(new Event(getChar(), getChar(), getBool()));
        }
        System.out.println(new Date());
        // for (int i = 0; i < ('z' - 'a') * 100; i++)
        // ep.getNext('a');
        System.out.println("table = " + ep.byChar('a'));
        // for (int i = 0; i < 100; i++)
        // System.out.println(" next = " + ep.getNext('a'));
        int[] vals = new int[26];
        for (int i = 0; i < 10000; i++)
            vals[ep.getNext('a') - 'a'] += 1;
        Collection<CalcedPair> cp = ep.byChar('a');
        Iterator<CalcedPair> icp = cp.iterator();
        for (int i = 0; i < vals.length; i++) {
            char c = (char) ('a' + i);
            System.out.println(c + " = " + vals[i] + "(" + icp.next().getValue() + ")");
        }
        System.out.println(new Date());
        System.out.println(ep.getAllTop());
        fail("Not yet implemented");
    }
}
