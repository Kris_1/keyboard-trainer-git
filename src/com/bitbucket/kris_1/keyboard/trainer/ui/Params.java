package com.bitbucket.kris_1.keyboard.trainer.ui;

import java.io.File;

public interface Params
{
    interface StringTo<T>
    {
        T convert(String str);

        final static StringTo<char[]> CHAR_ARRAY = new StringTo<char[]>()
        {
            @Override
            public char[] convert(String str) {
                return str.toCharArray();
            }
        };

        final static StringTo<Integer> INTEGER = new StringTo<Integer>()
        {
            @Override
            public Integer convert(String str) {
                return Integer.valueOf(str);
            }
        };

        final static StringTo<String> STRING = new StringTo<String>()
        {
            @Override
            public String convert(String str) {
                return str;
            }
        };

        final static StringTo<File> FILE = new StringTo<File>()
        {
            @Override
            public File convert(String str) {
                return new File(str);
            }
        };
    }

    public class Name<T>
    {
        final public static Name<char[]> LETTERS = new Name<char[]>("letters", StringTo.CHAR_ARRAY);
        final public static Name<Integer> MAX_EVENTS_NUMBER = new Name<Integer>("max-events-number", StringTo.INTEGER);
        final public static Name<Integer> TOP_WORST_EVENTS_NUMBER = new Name<Integer>(
            "top-worst-events-number",
            StringTo.INTEGER);
        final public static Name<File> EVENTS_FILE = new Name<File>("events-file", StringTo.FILE);
        final public static Name<Integer> TEST_CHAR_NUMBER = new Name<Integer>("test-char-number", StringTo.INTEGER);

        final private String name;
        final private StringTo<T> converter;

        public String getName() {
            return name;
        }

        public StringTo<T> getConverter() {
            return converter;
        }

        public Name(String name, StringTo<T> converter) {
            if (name == null)
                throw new NullPointerException("name");
            if (name.isEmpty())
                throw new IllegalArgumentException("name is empty");
            if (converter == null)
                throw new NullPointerException("converter");
            this.name = name;
            this.converter = converter;
        }
    }

    public <T> T get(Name<T> name);

    public void save();
}
