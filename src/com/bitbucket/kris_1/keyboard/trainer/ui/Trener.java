package com.bitbucket.kris_1.keyboard.trainer.ui;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.GroupBuilder;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ButtonBuilder;
import javafx.scene.layout.BorderPaneBuilder;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.StackPaneBuilder;
import javafx.stage.Stage;

import com.bitbucket.kris_1.keyboard.trainer.model.impl.EventProcessorImpl;
import com.bitbucket.kris_1.keyboard.trainer.ui.LetterPane.EndListener;
import com.bitbucket.kris_1.keyboard.trainer.ui.LetterProcessor.Statistic;

public class Trener extends Application
{
    final private Params params = new PropertyParams();
    private Scene scene;
    LetterProcessorImpl letterProcessor;

    private Node getTrainingScene() {
        EndListener endListener = new EndListener()
        {
            @Override
            public void end(LetterProcessor.Statistic statistic) {
                Node pane = getEndScene(statistic);
                ((StackPane) scene.getRoot()).getChildren().remove(0);
                ((StackPane) scene.getRoot()).getChildren().add(pane);
                ((StackPane) scene.getRoot()).getChildren().add(GroupBuilder.create().children(pane).build());
            }
        };
        return new LetterPane(letterProcessor, endListener);
    }

    private Node getEndScene(Statistic statistic) {
        StatisticPane grid = new StatisticPane();
        grid.add("Среднее время нажатия", String.format("%.2f мс", statistic.getAverageKeyShift()));
        grid.add("Отклонение в нажатии", String.format("±%.2f мс", statistic.getVariativity()));
        grid.add("Скорость", String.format("%.0f сим/мин", statistic.getSpeed()));
        grid.add("Процент ошибок", String.format("%.2f %%", statistic.getErrorPercent()));
        grid.setPadding((new Insets(10)));
        return BorderPaneBuilder //
            .create()
            .center(grid)
            .bottom(HBoxBuilder //
                .create()
                .alignment(Pos.CENTER_RIGHT)
                .padding(new Insets(10))
                .children(ButtonBuilder //
                    .create()
                    .text("Продолжить")
                    .onAction(new EventHandler<ActionEvent>()
                    {
                        @Override
                        public void handle(ActionEvent event) {
                            StackPane root = (StackPane) scene.getRoot();
                            root.getChildren().remove(0);
                            root.getChildren().add(getTrainingScene());
                        }
                    })
                    .build())
                .build())
            .build();
    }

    @Override
    public void start(final Stage stage) throws Exception {
        this.scene = new Scene(StackPaneBuilder //
            .create()
            .maxWidth(1000)
            .minWidth(1000)
            .padding(new Insets(20))
            .alignment(Pos.CENTER)
            .build());
        this.letterProcessor =
            new LetterProcessorImpl(
                new EventProcessorImpl(
                    params.get(Params.Name.LETTERS),
                    params.get(Params.Name.MAX_EVENTS_NUMBER),
                    params.get(Params.Name.TOP_WORST_EVENTS_NUMBER)),
                params.get(Params.Name.EVENTS_FILE),
                params.get(Params.Name.TEST_CHAR_NUMBER));
        ((StackPane) scene.getRoot()).getChildren().add(getTrainingScene());
        stage.setTitle("Trenager");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
        stage.centerOnScreen();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
