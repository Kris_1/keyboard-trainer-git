package com.bitbucket.kris_1.keyboard.trainer.ui;

public interface LetterProcessor
{
    public class PressKey
    {
        final private char before;
        final private char expected;
        final private char actual;
        final private long created = System.currentTimeMillis();

        public char getBefore() {
            return before;
        }

        public char getExpected() {
            return expected;
        }

        public char getActual() {
            return actual;
        }

        public long getCreated() {
            return created;
        }

        public PressKey(char before, char expected, char actual) {
            this.before = before;
            this.expected = expected;
            this.actual = actual;
        }

        @Override
        public String toString() {
            return "Event [before=" + before + ", expected=" + expected + ", actual=" + actual + "]";
        }
    }

    public class Statistic
    {
        final private double averageKeyShift;
        final private double variativity;
        final private double speed;
        final private double errorPercent;

        public double getAverageKeyShift() {
            return averageKeyShift;
        }

        public double getVariativity() {
            return variativity;
        }

        public double getSpeed() {
            return speed;
        }

        public double getErrorPercent() {
            return errorPercent;
        }

        public Statistic(double averageKeyShift, double variativity, double speed, double errorPercent) {
            this.averageKeyShift = averageKeyShift;
            this.variativity = variativity;
            this.speed = speed;
            this.errorPercent = errorPercent;
        }

        @Override
        public String toString() {
            return "Statistic [averageKeyShift="
                + averageKeyShift
                + ", variativity="
                + variativity
                + ", speed="
                + speed
                + ", errorPercent="
                + errorPercent
                + "]";
        }
    }

    int getCount();

    char getNext();

    boolean consume(PressKey event);

    void start();

    void first();

    Statistic finish();
}