package com.bitbucket.kris_1.keyboard.trainer.ui;

import javafx.geometry.HPos;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextBuilder;

public class StatisticPane extends GridPane
{
    private int row = 0;

    public void add(String label, String value) {
        this.add(//
            TextBuilder //
                .create()
                .text(label)
                .build(),
            0,
            row);
        Text val = TextBuilder //
            .create()
            .text(value)
            .build();
        this.add(val, 1, row);
        this.setHalignment(val, HPos.RIGHT);
        row += 1;
    }

    public StatisticPane() {
        setHgap(10);
        setVgap(10);
    }

}
