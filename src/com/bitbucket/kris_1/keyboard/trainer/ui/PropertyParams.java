package com.bitbucket.kris_1.keyboard.trainer.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;

public class PropertyParams implements Params
{
    private static final String TRENER_PROPERTIES_FILE = "trener.properties";

    final private String rootDir;
    final private Properties properties;

    @Override
    public <T> T get(Name<T> name) {
        return name.getConverter().convert(properties.getProperty(name.getName()));
    }

    @Override
    public void save() {
        try {
            File file = new File(rootDir + TRENER_PROPERTIES_FILE);
            try (OutputStream out = new FileOutputStream(file);) {
                properties.store(out, new Date().toString());
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public PropertyParams() {
        try {
            this.rootDir =
                new File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI().getPath())
                    .getParentFile()
                    .getAbsolutePath();
            this.properties = new Properties();
            try (InputStream in = getClass().getResourceAsStream("/_" + TRENER_PROPERTIES_FILE);) {
                properties.load(in);
            }
            properties.setProperty(
                Name.EVENTS_FILE.getName(),
                this.rootDir + "/" + properties.getProperty(Name.EVENTS_FILE.getName()));
            {
                File file = new File(rootDir + TRENER_PROPERTIES_FILE);
                if (file.exists())
                    try (InputStream in = new FileInputStream(file);) {
                        properties.load(in);
                    }
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public String toString() {
        return "PropertyParams [rootDir=" + rootDir + ", properties=" + properties + "]";
    }
}
