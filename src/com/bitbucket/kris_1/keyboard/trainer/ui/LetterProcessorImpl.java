package com.bitbucket.kris_1.keyboard.trainer.ui;

import static java.lang.Math.*;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import com.bitbucket.kris_1.keyboard.trainer.model.Event;
import com.bitbucket.kris_1.keyboard.trainer.model.EventProcessor;

public class LetterProcessorImpl implements LetterProcessor
{
    private static class FinishState implements LetterProcessor
    {
        @Override
        public int getCount() {
            throw new IllegalStateException("wrong state");
        }

        @Override
        public char getNext() {
            throw new IllegalStateException("wrong state");
        }

        @Override
        public boolean consume(PressKey event) {
            throw new IllegalStateException("wrong state");
        }

        @Override
        public void start() {
            throw new IllegalStateException("wrong state");
        }

        @Override
        public void first() {
            throw new IllegalStateException("wrong state");
        }

        @Override
        public Statistic finish() {
            throw new IllegalStateException("wrong state");
        }
    }

    private class StartState implements LetterProcessor
    {
        private class PressKeyInner
        {
            final private char before;
            final private char expected;
            final private char actual;
            final private int pressShift;

            public boolean isOk() {
                return expected == actual;
            }

            public PressKeyInner(PressKey pressKey) {
                if (pressKey == null)
                    throw new NullPointerException("pressKey");
                this.before = pressKey.getBefore();
                this.expected = pressKey.getExpected();
                this.actual = pressKey.getActual();
                long old = time;
                time = System.currentTimeMillis();
                this.pressShift = (int) (time - old);
            }

            @Override
            public String toString() {
                return "Event [before="
                    + before
                    + ", expected="
                    + expected
                    + ", actual="
                    + actual
                    + ", pressShift="
                    + pressShift
                    + "]";
            }
        }

        private class PressKeyNorm implements Comparable<PressKeyNorm>
        {
            final private PressKeyInner inner;
            final private double norm;

            public char getCharA() {
                return inner.before;
            }

            public char getCharB() {
                return inner.expected;
            }

            public char getActualB() {
                return inner.actual;
            }

            public boolean isOk() {
                return inner.isOk();
            }

            public double getNorm() {
                return norm;
            }

            @Override
            public int compareTo(PressKeyNorm o) {
                return (int) signum(norm - o.norm);
            }

            public PressKeyNorm(PressKeyInner inner, double norm) {
                if (inner == null)
                    throw new NullPointerException("inner");
                if (norm <= 0)
                    throw new IllegalArgumentException("norm <= 0");
                this.inner = inner;
                this.norm = norm;
            }
        }

        final private ArrayList<PressKeyInner> session = new ArrayList<>(1000);
        private long startTime;
        private long time = 0;
        private char[] chars = eventProcessor.getLesson(count);
        private int index = 0;

        @Override
        public int getCount() {
            return count;
        }

        @Override
        public boolean consume(PressKey event) {
            PressKeyInner ev = new PressKeyInner(event);
            session.add(ev);
            return ev.isOk();
        }

        @Override
        public char getNext() {
            return chars[index++];
        }

        @Override
        public void start() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void first() {
            time = System.currentTimeMillis();
            startTime = time;
        }

        private Collection<PressKeyNorm> toPressKeyNorms(Collection<PressKeyInner> pressKeys) {
            int max = 0;
            for (Iterator<PressKeyInner> i = pressKeys.iterator(); i.hasNext();)
                if (i.next().pressShift > 10000)
                    i.remove();
            for (PressKeyInner key : pressKeys)
                if (max < key.pressShift)
                    max = key.pressShift;
            ArrayList<PressKeyNorm> result = new ArrayList<>(pressKeys.size());
            for (PressKeyInner key : pressKeys)
                result.add(new PressKeyNorm(key, (double) key.pressShift / max));
            return result;
        }

        private Collection<PressKeyNorm> removeBounds(Collection<PressKeyNorm> norms) {
            int remove = norms.size() / 4;
            LinkedList<PressKeyNorm> result = new LinkedList<>(norms);
            Collections.sort(result);
            for (int i = 0; i < remove; i++) {
                result.removeFirst();
                result.removeLast();
            }
            return result;
        }

        private double[] calcVariance(Collection<PressKeyNorm> norms) {
            double expectedValue = 0;
            for (PressKeyNorm e : norms) {
                expectedValue += e.getNorm();
            }
            expectedValue = expectedValue / norms.size();
            double variance = 0;
            for (PressKeyNorm e : norms) {
                variance += (e.getNorm() - expectedValue) * (e.getNorm() - expectedValue);
            }
            variance = variance / norms.size();
            double secToPart = 0;
            for (Iterator<PressKeyNorm> i = norms.iterator(); i.hasNext();) {
                PressKeyNorm key = i.next();
                secToPart = key.inner.pressShift / key.getNorm();
                break;
            }
            double wrong = 0;
            for (Iterator<PressKeyNorm> i = norms.iterator(); i.hasNext();) {
                PressKeyNorm key = i.next();
                wrong += key.isOk() ? 0 : 1;
            }
            return new double[] { expectedValue, 3 * sqrt(variance), secToPart, wrong / norms.size() };
        }

        @Override
        public Statistic finish() {
            long endTime = System.currentTimeMillis();
            Collection<PressKeyNorm> norms = toPressKeyNorms(session);
            double[] stats = calcVariance(removeBounds(norms));
            double coeff = (stats[0] + stats[1]) / Math.log(0.25);
            ArrayList<Event> events = new ArrayList<>(session.size());
            for (PressKeyNorm e : norms) {
                if (!e.isOk())
                    continue;
                Event event = new Event(e.getCharA(), e.getCharB(), Math.exp(e.getNorm() / coeff));
                events.add(event);
            }
            eventProcessor.addEvents(events);
            File f = new File(file.getAbsolutePath() + ".tmp");
            try (ObjectOutputStream os =
                new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(f), 100 * 1024));) {
                os.writeObject(eventProcessor.getEvents());
                if (file.exists())
                    file.delete();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
            f.renameTo(file);
            return new Statistic( //
                stats[0] * stats[2],
                stats[1] * stats[2],
                session.size() / ((endTime - startTime) / 60000d),
                stats[3] * 100);
        }
    }

    final private EventProcessor eventProcessor;
    final private File file;
    final private int count;
    private LetterProcessor state;

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public char getNext() {
        return state.getNext();
    }

    @Override
    public boolean consume(PressKey event) {
        return state.consume(event);
    }

    @Override
    public void start() {
        state = new StartState();
    }

    @Override
    public void first() {
        state.first();
    }

    @Override
    public Statistic finish() {
        Statistic result = state.finish();
        state = new FinishState();
        return result;
    }

    public LetterProcessorImpl(EventProcessor eventProcessor, File file, int count) {
        if (eventProcessor == null)
            throw new NullPointerException("eventProcessor");
        if (file == null)
            throw new NullPointerException("file");
        if (count <= 0)
            throw new IllegalArgumentException("count <= 0");
        this.eventProcessor = eventProcessor;
        if (file.exists())
            try (ObjectInputStream in =
                new ObjectInputStream(new BufferedInputStream(new FileInputStream(file), 100 * 1024));) {
                @SuppressWarnings("unchecked")
                Collection<Event> events = (Collection<Event>) in.readObject();
                eventProcessor.addEvents(events);
                // System.out.println(eventProcessor);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        this.file = file;
        this.count = count;
    }
}
