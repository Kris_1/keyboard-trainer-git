package com.bitbucket.kris_1.keyboard.trainer.ui;

import javafx.animation.FadeTransitionBuilder;
import javafx.animation.FillTransitionBuilder;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransitionBuilder;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.FontBuilder;
import javafx.scene.text.FontSmoothingType;
import javafx.scene.text.Text;
import javafx.scene.text.TextBuilder;
import javafx.util.Duration;

import com.bitbucket.kris_1.keyboard.trainer.ui.LetterProcessor.PressKey;

public class LetterPane extends FlowPane
{
    final private static Font FONT = FontBuilder.create().size(21).name("Consolas").build();

    public interface EndListener
    {
        void end(LetterProcessor.Statistic statistic);
    }

    final private static class TextPane extends StackPane
    {
        final private TextPane prev;
        private TextPane next;
        final private char value;
        private Rectangle rect;

        public TextPane getPrev() {
            return prev;
        }

        public TextPane getNext() {
            return next;
        }

        public char getValue() {
            return value;
        }

        public void activate() {
            rect.setStroke(Color.gray(0.75));
            rect.setStrokeWidth(1);
            rect.setStrokeType(StrokeType.INSIDE);
            rect.setStrokeLineJoin(StrokeLineJoin.ROUND);
        }

        public void good() {
            rect.setStrokeWidth(0);
            ParallelTransitionBuilder. //
                create()
                .children(//
                    FadeTransitionBuilder //
                        .create()
                        .duration(Duration.millis(50))
                        .fromValue(0)
                        .toValue(0.3)
                        .build(),
                    FillTransitionBuilder //
                        .create()
                        .duration(Duration.millis(50))
                        .fromValue(Color.TRANSPARENT)
                        .toValue(Color.GREEN)
                        .build())
                .interpolator(Interpolator.EASE_OUT)
                .node(rect)
                .build()
                .play();
        }

        public void wrong() {
            ParallelTransitionBuilder. //
                create()
                .children(//
                    FadeTransitionBuilder //
                        .create()
                        .duration(Duration.millis(50))
                        .fromValue(0)
                        .toValue(0.2)
                        .build(),
                    FillTransitionBuilder //
                        .create()
                        .duration(Duration.millis(50))
                        .fromValue(Color.TRANSPARENT)
                        .toValue(Color.RED)
                        .build())
                .interpolator(Interpolator.EASE_IN)
                .node(rect)
                .build()
                .play();
        }

        public TextPane(TextPane prev, char value) {
            if (prev == null)
                throw new NullPointerException("prev");
            this.prev = prev;
            this.prev.next = this;
            this.value = value;
            Text text = TextBuilder //
                .create()
                .font(FONT)
                .fontSmoothingType(FontSmoothingType.LCD)
                .text(Character.valueOf(value).toString())
                .build();
            this.rect = RectangleBuilder //
                .create()
                .width(text.getLayoutBounds().getWidth() + 8)
                .height(text.getLayoutBounds().getHeight() + 2)
                .fill(Color.TRANSPARENT)
                .arcHeight(8)
                .arcWidth(8)
                .build();
            setPadding(new Insets(1));
            getChildren().addAll(rect, text);
        }

        private TextPane() {
            this.prev = null;
            this.next = null;
            this.rect = null;
            this.value = '\u0000';
        }

        final public static TextPane NULL = new TextPane();
    }

    private interface KeyProcess
    {
        void process(char key);
    }

    private class InitKeyProcess implements KeyProcess
    {
        final private TextPane init;

        @Override
        public void process(char key) {
            keyProcess = new NextKeyProcess(init.getNext());
            if (init.getValue() == key)
                init.good();
            else
                init.wrong();
            processor.first();
        }

        public InitKeyProcess(TextPane init) {
            if (init == null)
                throw new NullPointerException("init");
            this.init = init;
            init.activate();
        }
    }

    private class NextKeyProcess implements KeyProcess
    {
        private TextPane item;

        @Override
        public void process(char key) {
            long t = System.currentTimeMillis();
            boolean ok = processor.consume(new PressKey(item.getPrev().getValue(), item.getValue(), key));
            if (ok)
                item.good();
            else
                item.wrong();
            item = item.getNext();
            if (item == null) {
                endListener.end(processor.finish());
            } else {
                item.activate();
            }

        }

        public NextKeyProcess(TextPane item) {
            this.item = item;
            item.activate();
        }
    }

    final private LetterProcessor processor;
    final private EndListener endListener;
    private KeyProcess keyProcess;

    public LetterPane(final LetterProcessor processor, final EndListener endListener) {
        if (processor == null)
            throw new NullPointerException("processor");
        if (endListener == null)
            throw new NullPointerException("endListener");
        this.processor = processor;
        processor.start();
        this.endListener = endListener;
        setFocusTraversable(true);
        setPadding(new Insets(0));
        setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent ev) {
                if (MouseButton.PRIMARY == ev.getButton() && !LetterPane.this.isFocused())
                    LetterPane.this.requestFocus();
                ev.consume();
            }
        });
        setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent event) {
                event.consume();
                if (event.getCode() == KeyCode.TAB) {
                    // TODO focus to next element
                } else if (event.getCode() == KeyCode.ESCAPE) {
                    endListener.end(processor.finish());
                }
                if (event.getText() == null || event.getText().isEmpty())
                    return;
                keyProcess.process(event.getText().charAt(0));
            }
        });
        TextPane prev = TextPane.NULL;
        for (int i = 0; i < processor.getCount(); i++) {
            prev = new TextPane(prev, processor.getNext());
            getChildren().add(prev);
        }
        this.keyProcess = new InitKeyProcess((TextPane) getChildren().get(0));
    }
}
