package com.bitbucket.kris_1.keyboard.trainer.model.impl;

import static java.lang.Math.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.bitbucket.kris_1.keyboard.trainer.model.Event;
import com.bitbucket.kris_1.keyboard.trainer.model.EventProcessor;

public class EventProcessorImpl implements EventProcessor
{
    final private int max;
    final private int topSize;
    final private LinkedList<Event> queue = new LinkedList<>();
    final private Map<Event.Key, CalcedPair> events = new HashMap<>();
    final private Map<Character, List<CalcedPair>> byChar = new HashMap<>();

    private void addEvent(Event event) {
        if (Double.isInfinite(event.getResult()) || Double.isNaN(event.getResult()))
            return;
        CalcedPair cp = events.get(event.getKey());
        if (cp == null)
            return;
        cp.add(event);
        queue.addLast(event);
        if (queue.size() > max) {
            Event ev = queue.removeFirst();
            events.get(ev.getKey()).remove(ev);
        }
    }

    @Override
    public void addEvents(Collection<Event> events) {
        for (Event event : events)
            addEvent(event);
    }

    @Override
    public Collection<Event> getEvents() {
        return Collections.unmodifiableCollection(queue);
    }

    public char getNextDirty(char prev, CachedLetter letters) {
        double d = random();
        double u = 0;
        List<CachedLetter.Pair> pairs = letters.getPair(prev);
        for (CachedLetter.Pair pair : pairs) {
            u += pair.getValue();
            if (u > d)
                return pair.getLetter();
        }
        throw new IllegalStateException("next fo char " + prev + " is not found(" + pairs + ")");
    }

    private List<CalcedPair> getTop() {
        ArrayList<CalcedPair> top = new ArrayList<>(events.values());
        Collections.sort(top);
        ArrayList<CalcedPair> result = new ArrayList<>(topSize);
        Iterator<CalcedPair> it = top.iterator();
        for (int i = 0; i < topSize; i++)
            result.add(it.next());
        return result;
    }

    @Override
    public char[] getLesson(int size) {
        List<CalcedPair> top = getTop();
        CachedLetter cLetters = new CachedLetter(byChar);
        char[] result = new char[size];
        char prev = top.get(0).getKey().getA();
        result[0] = prev;
        for (int i = 1; i < size; i++) {
            if (random() > 0.3) {
                result[i] = getNextDirty(prev, cLetters);
                prev = result[i];
            } else {
                int index = (int) ((top.size()) * random());
                Event.Key key = top.get(index).getKey();
                result[i] = key.getA();
                i += 1;
                if (i < size) {
                    result[i] = key.getB();
                    prev = result[i];
                }
            }
        }
        return result;
    }

    public EventProcessorImpl(char[] chars, int max, final int topSize) {
        if (max <= 0)
            throw new IllegalArgumentException("max <= 0");
        if (topSize <= 0)
            throw new IllegalArgumentException("topSize <= 0");
        this.max = max;
        this.topSize = topSize;
        for (char i : chars) {
            ArrayList<CalcedPair> pairs = new ArrayList<>(chars.length);
            for (char j : chars) {
                if (j == i)
                    continue;
                Event.Key key = new Event.Key(i, j);
                CalcedPair pair = new CalcedPair(key);
                pairs.add(pair);
                events.put(key, pair);
            }
            byChar.put(i, pairs);
        }
    }

    @Override
    public String toString() {
        return events.values().toString();
    }

    public static char[] of(char from, char to) {
        char[] result = new char[to - from + 1];
        int j = 0;
        for (char i = from; i <= to; i++, j++)
            result[j] = i;
        return result;
    }
}
