package com.bitbucket.kris_1.keyboard.trainer.model.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CachedLetter
{
    final public static class Pair
    {
        final private char letter;
        private double value;

        public char getLetter() {
            return letter;
        }

        public double getValue() {
            return value;
        }

        public Pair(char letter, double value) {
            super();
            this.letter = letter;
            this.value = value;
        }

        @Override
        public String toString() {
            return "Pair [letter=" + letter + ", value=" + value + "]";
        }
    }

    final private Map<Character, List<CalcedPair>> byChar;
    final private HashMap<Character, List<Pair>> calcedChar = new HashMap<>();

    private double[] getMinMax(Collection<CalcedPair> pairs) {
        double[] result = new double[2];
        Iterator<CalcedPair> i = pairs.iterator();
        CalcedPair pair = i.next();
        result[0] = pair.getDiff();
        result[1] = pair.getDiff();
        while (i.hasNext()) {
            pair = i.next();
            if (result[0] > pair.getDiff())
                result[0] = pair.getDiff();
            if (result[1] < pair.getDiff())
                result[1] = pair.getDiff();
        }
        return result;
    }

    public List<Pair> getPair(char c) {
        List<Pair> result = calcedChar.get(c);
        if (result != null)
            return result;
        List<CalcedPair> cPairs = byChar.get(c);
        double[] minMax = getMinMax(cPairs);
        ArrayList<Pair> pairs = new ArrayList<>(cPairs.size());
        if (minMax[0] - minMax[1] < 0.01) {
            for (CalcedPair cPair : cPairs)
                pairs.add(new Pair(cPair.getKey().getB(), 1d / cPairs.size()));
        } else {
            for (CalcedPair cPair : cPairs)
                pairs.add(new Pair(cPair.getKey().getB(), minMax[1] - cPair.getDiff()));
            double sum = 0;
            for (Pair pair : pairs)
                sum += pair.getValue();
            for (Pair pair : pairs)
                pair.value = pair.value / sum;
        }
        calcedChar.put(c, pairs);
        return pairs;
    }

    public CachedLetter(Map<Character, List<CalcedPair>> byChar) {
        if (byChar == null)
            throw new NullPointerException("byChar");
        this.byChar = byChar;
    }
}
