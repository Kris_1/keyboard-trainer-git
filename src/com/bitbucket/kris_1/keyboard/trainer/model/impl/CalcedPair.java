package com.bitbucket.kris_1.keyboard.trainer.model.impl;

import static java.lang.Math.*;

import java.util.ArrayList;

import com.bitbucket.kris_1.keyboard.trainer.model.Event;

public class CalcedPair implements Comparable<CalcedPair>
{
    private double real;
    final private Event.Key key;
    final private ArrayList<Event> events;

    public Event.Key getKey() {
        return key;
    }

    private void changeDiff(double value) {
        real += value;
    }

    public double getDiff() {
        return real;
    }

    @Override
    public int compareTo(CalcedPair o) {
        return (int) signum(real - o.real);
    }

    private void check(Event event) {
        if (!key.equals(event.getKey()))
            throw new IllegalArgumentException("!key.equals(event.getKey()) key = " + key + ", event = " + event);
    }

    public void add(Event event) {
        check(event);
        events.add(event);
        changeDiff(event.getResult());
    }

    public void remove(Event event) {
        check(event);
        events.remove(event);
        changeDiff(-event.getResult());
    }

    public CalcedPair(Event.Key key) {
        if (key == null)
            throw new NullPointerException("key");
        this.key = key;
        this.events = new ArrayList<>(1000);
        changeDiff(-100);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + key.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        CalcedPair other = (CalcedPair) obj;
        return key.equals(other.key);
    }

    @Override
    public String toString() {
        return "CalcedPair [real=" + real + ", key=" + key + "]";
    }
}
