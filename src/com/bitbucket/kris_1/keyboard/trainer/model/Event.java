package com.bitbucket.kris_1.keyboard.trainer.model;

import java.io.Serializable;
import java.util.Date;

public class Event implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static class Key implements Serializable
    {
        private static final long serialVersionUID = 1L;

        private char a;
        private char b;

        public char getA() {
            return a;
        }

        public char getB() {
            return b;
        }

        public Key(char a, char b) {
            this.a = a;
            this.b = b;
        }

        @Override
        public String toString() {
            return "Key [a=" + a + ", b=" + b + "]";
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + a;
            result = prime * result + b;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null || getClass() != obj.getClass())
                return false;
            Key other = (Key) obj;
            return a == other.a && b == other.b;
        }
    }

    private Key key;
    private Date date;
    private double result;

    public Key getKey() {
        return key;
    }

    public char getA() {
        return key.getA();
    }

    public char getB() {
        return key.getB();
    }

    public Date getDate() {
        return date;
    }

    public double getResult() {
        return result;
    }

    public Event(char a, char b, double result) {
        this.key = new Key(a, b);
        this.date = new Date();
        this.result = result;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + key.hashCode();
        result = prime * result + date.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        Event other = (Event) obj;
        return key.equals(other.key) && date.equals(other.date);
    }

    @Override
    public String toString() {
        return "Event [key=" + key + ", date=" + date + ", result=" + result + "]";
    }
}
