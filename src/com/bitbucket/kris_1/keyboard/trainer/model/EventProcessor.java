package com.bitbucket.kris_1.keyboard.trainer.model;

import java.util.Collection;

public interface EventProcessor
{
    Collection<Event> getEvents();

    void addEvents(Collection<Event> events);

    char[] getLesson(int size);
}
